#ifndef IMGUI_DEBUGCONSOLE_H
#define IMGUI_DEBUGCONSOLE_H

#include <functional>
#include <stdio.h>

#ifdef _MSC_VER
# include <sal.h>
# if _MSC_VER > 1400
#  define FORMAT_STRING(p) _Printf_format_string_ p
# else
#  define FORMAT_STRING(p) __format_string p
# endif /* FORMAT_STRING */
#else
# define FORMAT_STRING(p) p
#endif

namespace ImGuiDebugConsole {
	enum MessageType : char {
		DEBUG = 1,
		INFO = 2,
		WARNING = 3,
		ERROR = 4,
		FATAL = 5,
		TRACE = 6,
		COMMAND = 7,
		NONE = 8
	};

	typedef struct CommandData {
		char* CommandKey;
		char* Description;
		char* Arguments;
		std::function<void(int, char**)> Function;

		CommandData(char* command, char* arguments, char* description);
	} CommandData;

	bool Show();

	void Open();

	void Close();

	void Toggle();

	void SetConsoleDisplayLevel(MessageType level);

#ifdef _MSC_VER
	void Debug(FORMAT_STRING(const char* fmt), ...);

	void Info(FORMAT_STRING(const char* fmt), ...);

	void Warning(FORMAT_STRING(const char* fmt), ...);

	void Error(FORMAT_STRING(const char* fmt), ...);

	void Fatal(FORMAT_STRING(const char* fmt), ...);

	void Trace(FORMAT_STRING(const char* fmt), ...);

	void Command(FORMAT_STRING(const char* fmt), ...);

	void Text(FORMAT_STRING(const char* fmt), ...);
#else
	void Debug(const char* fmt, ...)__attribute__ ((format (printf, 1, 2)));

	void Info(const char* fmt, ...)__attribute__ ((format (printf, 1, 2)));

	void Warning(const char* fmt, ...)__attribute__ ((format (printf, 1, 2)));

	void Error(const char* fmt, ...)__attribute__ ((format (printf, 1, 2)));

	void Fatal(const char* fmt, ...)__attribute__ ((format (printf, 1, 2)));

	void Trace(const char* fmt, ...)__attribute__ ((format (printf, 1, 2)));

	void Command(const char* fmt, ...)__attribute__ ((format (printf, 1, 2)));

	void Text(const char* fmt, ...)__attribute__ ((format (printf, 1, 2)));
#endif

	void RegisterCommand(const char* command, const char* arguments, const char* description, const std::function<void(int, char**)>& commandFunction);

	void ExecuteCommand(const char* command);

	void ClearCommands();

	char* DefaultFormatter(MessageType type, bool ansiFormat, const char* fmt, va_list& args);

	void SetAnsiFormatter(const std::function<char*(MessageType type, bool ansiFormat, const char* fmt, va_list& args)>& customFormatter);

	void SetExternalOutput(const std::function<void(MessageType type, const char*)>& output);

	void SetExternalAnsiOutput(const std::function<void(MessageType type, const char*)>& output);

	void Initialize(float width, float height, float x, float y, const char* title = "Debug Console");

	void DeInitialize();

	bool HasFocus();

	void Clear();

	CommandData* GetCommands(int* count);
}
#endif //IMGUI_DEBUGCONSOLE_H
