#include "imGui-debugConsole/imGui-debugConsole.h"
#include "imGui-debugConsole/imGuiAnsiColor.hpp"

#include <vector>
#include <string>
#include <cstring>
#include <stdexcept>
#include <imgui.h>
#include <iostream>

#ifdef _MSC_VER
#include <windows.h>
#endif

#define INPUT_BUFFER 128
#define HINT_BUFFER 256

namespace ImGuiDebugConsole {
    std::string messages = std::string();
    std::vector<CommandData> commands = std::vector<CommandData>();

    std::function<char*(MessageType type, bool ansiFormat, const char* fmt, va_list &args)> formatter = nullptr;
    std::function<void(MessageType type, const char*)> externalAnsiOutput = nullptr;
    std::function<void(MessageType type, const char*)> externalOutput = nullptr;

    bool isShown = false;
    bool autoScroll = true;
    bool hasFocus = false;

    float _x;
    float _y;
    float _width;
    float _height;

    char* _title;
    char* inputBuf;

    MessageType _displayLevel = MessageType::INFO;

    void ProcessMessage(const ImGuiDebugConsole::MessageType &type, const char* fmt, va_list &args) {
        va_list argsCopy;
        va_copy(argsCopy, args);
        auto text = formatter(type, true, fmt, args);

        if (text != nullptr) {
            if (_displayLevel <= type) {
                messages.append(text);
                messages.append("\n");
            }

            if (type != NONE) {
                if (externalAnsiOutput != nullptr) {
                    externalAnsiOutput(type, text);
                }

                if (externalOutput != nullptr) {
                    auto temp = formatter(type, false, fmt, argsCopy);
                    if (temp != nullptr) {
                        externalOutput(type, temp);
                        free(temp);
                    }
                }
            }
        }

        va_end(argsCopy);
    }

    int UpdateHint(ImGuiInputTextCallbackData* data) {
        return 1;
    }

    std::vector<char*> SplitArgs(const char* input, int* count, char** command) {
        auto isCommand = false;
        std::string buff;
        auto output = std::vector<char*>();

        auto size = std::strlen(input);

        if (size > INPUT_BUFFER) {
            size = INPUT_BUFFER;
        }

        auto handleBuff = [&output, &buff](int* count) {
            if (buff.empty()) {
                return;
            }
            count[0]++;

            auto temp = (char*)calloc(sizeof(char), buff.size() + 1);
            std::strcpy(temp, buff.c_str());

            output.push_back(temp);
            buff.clear();
        };

        auto handleCommand = [&buff, &isCommand](char** command) {
            isCommand = true;
            command[0] = (char*)calloc(1, buff.size() + 1);
            std::strcpy(command[0], buff.c_str());
            buff.clear();
        };

        for (auto i = 0 ; i < size ; i++) {
            auto c = input[i];

            if (c == ' ') {
                if (!isCommand) {
                    handleCommand(command);
                }

                handleBuff(count);
            } else if (c == '\"') {
                i++;
                while (input[i] != '\"' && i < size) {
                    c = input[i];
                    buff += c;
                    i++;
                }

                handleBuff(count);
            } else {
                buff += c;
            }
        }

        if (!isCommand) {
            handleCommand(command);
        } else {
            handleBuff(count);
        }

        return output;
    }

    void Render() {
        ImGui::Begin(_title, &isShown, ImGuiWindowFlags_NoCollapse);
        auto childSize = ImGui::GetWindowSize();
        childSize.y -= 60;
        childSize.x -= 20;
        ImGui::BeginChild("", childSize, false, ImGuiWindowFlags_AlwaysVerticalScrollbar | ImGuiWindowFlags_NoMove);
        ImGui::PushStyleColor(ImGuiCol_FrameBg, ImGui::GetColorU32(ImGuiCol_WindowBg));
        std::string line;
        std::stringstream buffer(messages);

        while (getline(buffer, line, '\n')) {
            ImGui::TextAnsiColored(ImVec4{ 1.0, 1.0, 1.0, 1.0 }, "%s", line.c_str());
        }

        buffer.clear();

        if (autoScroll && ImGui::GetScrollY() >= ImGui::GetScrollMaxY()) {
            ImGui::SetScrollHereY(1.0);
        }

        ImGui::PopStyleColor();
        ImGui::EndChild();
        ImGui::SetNextItemWidth(childSize.x);

        if (ImGui::InputTextWithHint("", nullptr, inputBuf, INPUT_BUFFER, ImGuiInputTextFlags_EnterReturnsTrue, UpdateHint)) {
            Text("%s", inputBuf);
            ExecuteCommand(inputBuf);
            memset(inputBuf, 0, std::strlen(inputBuf));
            ImGui::SetKeyboardFocusHere();
        }

        hasFocus = ImGui::IsItemFocused();

        ImGui::End();
    }
}

ImGuiDebugConsole::CommandData::CommandData(char* command, char* arguments, char* description) {
    CommandKey = command;
    Description = description;
    Arguments = arguments;
}

void ImGuiDebugConsole::Initialize(float x, float y, float width, float height, const char* title) {
    _x = x;
    _y = y;
    _width = width;
    _height = height;

    if (_title != nullptr) {
        free(_title);
        _title = nullptr;
    }

    if (inputBuf != nullptr) {
        free(inputBuf);
        inputBuf = nullptr;
    }

    auto titleSize = std::strlen(title);

    if (titleSize > 63) {
        titleSize = 63;
    }

    _title = (char*)calloc(1, titleSize + 1);
    std::strcpy(_title, title);

    inputBuf = (char*)calloc(1, INPUT_BUFFER);
}

void ImGuiDebugConsole::DeInitialize() {
    Clear();
    ClearCommands();
}

bool ImGuiDebugConsole::Show() {
    if (!isShown) {
        return false;
    }

    ImGui::SetNextWindowPos({ _x, _y }, ImGuiCond_Once);
    ImGui::SetNextWindowSize({ _width, _height }, ImGuiCond_Once);

    Render();

    return hasFocus;
}

void ImGuiDebugConsole::Open() {
    isShown = true;
}

void ImGuiDebugConsole::Close() {
    isShown = false;
}

void ImGuiDebugConsole::Toggle() {
    isShown = !isShown;
}

void ImGuiDebugConsole::Debug(const char* const fmt, ...) {
    if (formatter == nullptr) {
        throw std::runtime_error("Formatter has not been defined.");
    }

    va_list args;
    va_start(args, fmt);
    ProcessMessage(MessageType::DEBUG, fmt, args);
    va_end(args);
}

void ImGuiDebugConsole::Info(const char* const fmt, ...) {
    if (formatter == nullptr) {
        throw std::runtime_error("Formatter has not been defined.");
    }

    va_list args;
    va_start(args, fmt);
    ProcessMessage(MessageType::INFO, fmt, args);
    va_end(args);
}

void ImGuiDebugConsole::Warning(const char* const fmt, ...) {
    if (formatter == nullptr) {
        throw std::runtime_error("Formatter has not been defined.");
    }

    va_list args;
    va_start(args, fmt);
    ProcessMessage(MessageType::WARNING, fmt, args);
    va_end(args);
}

void ImGuiDebugConsole::Error(const char* const fmt, ...) {
    if (formatter == nullptr) {
        throw std::runtime_error("Formatter has not been defined.");
    }

    va_list args;
    va_start(args, fmt);
#ifdef _MSC_VER
#undef ERROR
#endif
    ProcessMessage(ImGuiDebugConsole::MessageType::ERROR, fmt, args);
    va_end(args);
}

void ImGuiDebugConsole::Fatal(const char* const fmt, ...) {
    if (formatter == nullptr) {
        throw std::runtime_error("Formatter has not been defined.");
    }

    va_list args;
    va_start(args, fmt);
    ProcessMessage(MessageType::FATAL, fmt, args);
    va_end(args);
}

void ImGuiDebugConsole::Trace(const char* fmt, ...) {
    if (formatter == nullptr) {
        throw std::runtime_error("Formatter has not been defined.");
    }

    va_list args;
    va_start(args, fmt);
    ProcessMessage(MessageType::TRACE, fmt, args);
    va_end(args);
}

void ImGuiDebugConsole::Command(const char* const fmt, ...) {
    if (formatter == nullptr) {
        throw std::runtime_error("Formatter has not been defined.");
    }

    va_list args;
    va_start(args, fmt);
    ProcessMessage(MessageType::COMMAND, fmt, args);
    va_end(args);
}

void ImGuiDebugConsole::Text(const char* fmt, ...) {
    if (formatter == nullptr) {
        throw std::runtime_error("Formatter has not been defined.");
    }

    va_list args;
    va_start(args, fmt);
    ProcessMessage(MessageType::NONE, fmt, args);
    va_end(args);
}


void ImGuiDebugConsole::RegisterCommand(const char* command, const char* arguments, const char* description, const std::function<void(int, char**)> &commandFunction) {
    for (auto &comm: commands) {
        if (std::strcmp(comm.CommandKey, command) == 0) {
            Error("Command %s already exists", comm.CommandKey);
            return;
        }
    }

    char* carguments = nullptr;

    auto ccommand = (char*)calloc(1, strlen(command) + 1);
    auto cdescription = (char*)calloc(1, strlen(description) + 1);
    if (arguments != nullptr && std::strlen(arguments) > 0) {
        carguments = (char*)calloc(1, strlen(arguments) + 1);
        std::strcpy(carguments, arguments);
    }
    std::strcpy(ccommand, command);
    std::strcpy(cdescription, description);

    auto com = CommandData(ccommand, carguments, cdescription);
    com.Function = commandFunction;

    commands.push_back(com);
}

void ImGuiDebugConsole::ExecuteCommand(const char* command) {
    int count = 0;
    char* commandId = nullptr;
    auto args = SplitArgs(command, &count, &commandId);
    if (commandId != nullptr) {
        for (auto &comm: commands) {
            if (std::strcmp(commandId, comm.CommandKey) == 0) {
                comm.Function(count, args.data());
                return;
            }
        }
    }

    for (auto i = 0 ; i < count ; i++) {
        free(args[i]);
    }

    Error("Command \"%s\" was not found", command);
}

void ImGuiDebugConsole::ClearCommands() {
    for (auto &command: commands) {
        free(command.CommandKey);
        free(command.Description);
    }
    commands.clear();
}

char* ImGuiDebugConsole::DefaultFormatter(ImGuiDebugConsole::MessageType type, bool ansiFormat, const char* fmt, va_list &args) {
    auto output = std::string();

    if (type != NONE) {
        output.append("[");

        switch (type) {
            case DEBUG:
                if (ansiFormat) {
                    output.append("\033[38;5;10m");
                }
                output.append("Debug");
                break;
            case INFO:
                if (ansiFormat) {
                    output.append("\033[38;5;45m");
                }
                output.append("Info");
                break;
            case WARNING:
                if (ansiFormat) {
                    output.append("\033[38;5;190m");
                }
                output.append("Warning");
                break;
            case ERROR:
                if (ansiFormat) {
                    output.append("\033[38;5;196m");
                }
                output.append("Error");
                break;
            case FATAL:
                if (ansiFormat) {
                    output.append("\033[38;5;13m");
                }
                output.append("Fatal");
                break;
            case TRACE:
                if (ansiFormat) {
                    output.append("\033[38;5;148m");
                }
                output.append("Trace");
                break;
            case COMMAND:
                if (ansiFormat) {
                    output.append("\033[38;5;252m");
                }
                output.append("Command");
                break;
        }

        if (ansiFormat) {
            output.append("\033[0m");
        }

        output.append("] ");
    }

    auto temp = std::vector<char>{};
    auto length = std::size_t{ 127 };

    while (temp.size() <= length) {
        temp.resize(length + 1);
        const auto status = std::vsnprintf(temp.data(), temp.size(), fmt, args);
        if (status < 0)
            throw std::runtime_error{ "string formatting error" };
        length = static_cast<std::size_t>(status);
    }

    output.append(temp.data(), length);
    temp.clear();

    auto outChar = (char*)calloc(1, output.size() + 1);
    std::memcpy(outChar, output.c_str(), output.size() + 1);
    output.clear();

    return outChar;
}

void ImGuiDebugConsole::SetAnsiFormatter(const std::function<char*(MessageType, bool, const char*, va_list &)> &customFormatter) {
    formatter = customFormatter;
}

void ImGuiDebugConsole::SetExternalOutput(const std::function<void(MessageType type, const char*)> &output) {
    externalOutput = output;
}

void ImGuiDebugConsole::SetExternalAnsiOutput(const std::function<void(MessageType type, const char*)> &output) {
#ifdef _MSC_VER
    auto hOut = GetStdHandle(STD_OUTPUT_HANDLE);

    DWORD dwMode = 0;
    GetConsoleMode(hOut, &dwMode);

    dwMode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING;

    SetConsoleMode(hOut, dwMode);
#endif
    externalAnsiOutput = output;
}

void ImGuiDebugConsole::Clear() {
    messages.clear();
}

ImGuiDebugConsole::CommandData* ImGuiDebugConsole::GetCommands(int* count) {
    count[0] = (int)commands.size();

    return commands.data();
}

bool ImGuiDebugConsole::HasFocus() {
    return hasFocus && isShown;
}

void ImGuiDebugConsole::SetConsoleDisplayLevel(ImGuiDebugConsole::MessageType level) {
    _displayLevel = level;
}