cmake_minimum_required(VERSION 3.16...3.17)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_C_STANDARD 11)

project(imGui-debugConsole VERSION 0.0.1)

include_directories(
        include
        dependencies/imgui/
)

set(IMGUI_CONSOLE_SRC
        src/imGui-debugConsole.cpp
    )

set(IMGUI_SRC
    ${CMAKE_CURRENT_SOURCE_DIR}/dependencies/imgui/imgui.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/dependencies/imgui/imgui_demo.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/dependencies/imgui/imgui_draw.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/dependencies/imgui/imgui_tables.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/dependencies/imgui/imgui_widgets.cpp
    )

add_library(imGui-DebugConsole STATIC ${IMGUI_CONSOLE_SRC} ${IMGUI_SRC})
